package com.training.staff.dao;

import com.training.staff.model.Employee;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeRepo extends MongoRepository<Employee, String> {

}